/** Alarms.
 *
 * @file Alarms.cpp
 *
 * This is the Alarms software module.
 * It handles the Alarms control loop.
 */

#include "Alarms.h"

/**
 * @brief Construct a new Alarms object
 *
 * @param sensors
 */
Alarms::Alarms(Sensors *sensors)
{

  _init(sensors);
}

/*!
 *  @brief  Initialize Alarms
 *
 *  @param sensors
 */
void Alarms::_init(Sensors *sensors)
{
  /* Set configuration parameters */
  _sensors = sensors;
}

/**
 * @brief Start alarms
 */
void Alarms::start()
{
  _running = true;
}

/**
 * @brief Stop alarms
 */
void Alarms::stop()
{
  _running = false;
}

/*!
 *  @brief  Check maximum pressure selected and general overpressure limit
 *
 *  @param pip maximum pressure value measured, in cmH2O
 */
void Alarms::evaluatePressure(short pip)
{
  SensorPressureValues_t pressures = _sensors->getRelativePressure();
  _currentPressure = pressures.pressure1;
  if (_currentPressure > pip + ALARM_MAX_PRESSURE_OVER_PIP)
  {
    buzzerOn();
    _currentAlarm = Alarm_Pressure_Over_Pip;
  }
  else if (_currentPressure > ALARM_OVERPRESSURE)
  {
    buzzerOn();
    _currentAlarm = Alarm_Overpressure;
  }
  else if (_currentPressure < ALARM_NO_PRESSURE)
  {
    buzzerOn();
    _currentAlarm = Alarm_No_Pressure;
  }
  else
  {
    if (_currentAlarm != No_Alarm)
    {
      buzzerOff();
      _currentAlarm = No_Alarm;
    }
  }
}

/*!
 *  @brief  Get flow reading from sensors
 *
 *  @warning Uncompleted function. No action taken after reading.
 */
void Alarms::evaluateFlow()
{
  _currentFlow = _sensors->getFlow();
}

/*!
 *  @brief  Mute alarms
 *
 *  @warning Mutes all alarms, regardless of their nature.
 */
void Alarms::muteAlarm()
{
  _alarmIsMuted = true;
}

/*!
 *  @brief  Unmute alarms
 */
void Alarms::unmuteAlarm()
{
  _alarmIsMuted = false;
}

/*!
 *  @brief  Get current alarm
 */
Alarm Alarms::readAlarm()
{
  return _currentAlarm;
}

/*!
 *  @brief Power on buzzer if alarm system is running and unmuted
 */
void Alarms::buzzerOn()
{
  if (_alarmIsMuted || !_running)
  {
    digitalWrite(PIN_BUZZ, LOW);
    return;
  }
  digitalWrite(PIN_BUZZ, HIGH);
}

/*!
 *  @brief Power off buzzer
 */
void Alarms::buzzerOff()
{
  digitalWrite(PIN_BUZZ, LOW);
}

/**
 * @brief Update alarms
 *
 * @see timer1Isr(void)
 */
void Alarms::update(short pip)
{
  evaluatePressure(pip);
  evaluateFlow();
}
